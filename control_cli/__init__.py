"""
Created on 2016-10-13

@author: Samir Khellaf

control_cli: CLI control for Python. Main module and base clases.

Copyright (C) 2016 Samir Khellaf <opensource@prosi-tech.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# Imports to make code work on both python 2 & 3
from __future__ import absolute_import, division, print_function
from six import class_types, integer_types, string_types, text_type, binary_type
from six.moves import input

# Imports required by the current module
from getpass import getpass
import re
from select import select
import socket

# Logging
import logging
logging.basicConfig(level=logging.WARN)

__all__ = ['CLI', 'TimeoutError', 'EOFError']

class CLIError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        if isinstance(self.value, string_types): return self.value
        else: return repr(self.value)

class TimeoutError(CLIError): pass
class EOFError(CLIError): pass
class TelnetError(CLIError): pass
class SerialError(CLIError): pass
class SSHError(CLIError): pass

class CLI(object):
    """
    Generic CLI class to communicate with a command line interface using
    Telnet, Serial, or SSH protocols.
    """
    # Class properties:
    fastCheckTimeout = 0.2
    irsRetries = 3
    def __init__(
            self, connectionType,
            host=None,
            port=None,
            timeout=10,
            echo=True,
            outputRecordSeparator='\n',
            prompt=r'.*[\?\$%#>]\s?$',
            userPrompt=r'(?:(?i)username|login)[: ]+$',
            passwordPrompt=r'(?i)password[: ]+$',
            username=None,
            password=None,
            **kwargs):
        """Constructor"""
        # Compile prompt REs if needed
        if isinstance(prompt, string_types): prompt = re.compile(prompt)
        if isinstance(userPrompt, string_types):
            userPrompt = re.compile(userPrompt)
        if isinstance(passwordPrompt, string_types):
            passwordPrompt = re.compile(passwordPrompt)

        # Create object properties for required options
        self.timeout = timeout
        self.ors = outputRecordSeparator
        self.prompt = prompt
        self.userPrompt = userPrompt
        self.passwordPrompt = passwordPrompt
        self.echo = echo
        self.username = username
        self.password = password

        # Create Serial connection _BaseIO object
        if(connectionType == 'Serial'):
            self.exp = Serial(port, timeout=timeout, **kwargs)

        # Create Telnet connection _BaseIO object
        elif(connectionType == 'Telnet'):
            if port == None: port = 23
            self.exp = Telnet(host, port, timeout)

        # Create SSH connection _BaseIO object
        elif(connectionType == 'SSH'):
            if port == None: port = 22
            self.exp = SSH(host, port=port, username=username,
                    password=password, **kwargs)
            self.exp.shell.settimeout(timeout)

        # Unknown connection
        else:
            raise CLIError(
                    "Unknown connectionType: '{}' ...!".format(connectionType))

        # Transfer timeout value to _BaseIO object
        self.exp.timeout = timeout

        # Quick check for the prompt or send an ORS and check again
        result = self.getPrompt(nofail=True, timeout=CLI.fastCheckTimeout)
        if result == None:
            logging.debug('Could not find a prompt...! sending an ORS ...')
            self.exp.send(self.ors)
            self.getPrompt()

        # Auto detect the IRS "Input Record Separator" or raise an error
        for i in range(0, CLI.irsRetries):
            self.exp.send(self.ors)
            self.irs = self.getPrompt()
            if(re.search(r'^\s+$', self.irs)): break
        if(i >= CLI.irsRetries):
            raise CLIError('Unable to identify an input record separator ...!')

    # getPrompt Method
    def getPrompt(self, nofail=False, timeout=None):
        """
        This method waits for the prompt for timeout amount of time.
        It asserts on timeout unless the nofail option is set to True.
        The method returns the content of the interface buffer less the prompt.
        """
        # Set default timeout
        if timeout == None: timeout = self.timeout

        # Function to execute on loginPrompt
        def _login(exp):
            if self.username == None:
                self.username = input('\nlogin: ')
            logging.debug('Sending user name:', self.username, '... ')
            self.exp.send(self.username + self.ors)

            return False

        # Function to execute on passwordPrompt
        def _password(exp):
            if self.password == None:
                self.password = getpass('password: ')
            logging.debug('Sending password', '... ')
            self.exp.send(self.password + self.ors)

            return False

        # Function to execute on prompt
        def _getResult(exp):
            if self.exp.consumed != None:
                # Remove prompt from the result
                self._result = self.exp.result
            else:
                self._result = None

            return True

        # Function to execute on timeout
        def _timeout(exp):
            if nofail:
                logging.debug('TIMED OUT after {}s ...!'.format(timeout))
                pass
            else:
                raise TimeoutError('TIMED OUT after {}s ...!'.format(timeout))

            return True

        # Function to execute on EOF encounter
        def _endOfFile(exp):
            raise EOFError('EOF...!')

        # Initialize result to None
        self._result = None

        # Expect any defined prompts
        self.exp.expect([
            (self.userPrompt, _login),
            (self.passwordPrompt, _password),
            (self.prompt, _getResult)],
            timeout=timeout,
            onEOF=_endOfFile,
            onTimeout=_timeout)

        # Return result
        return(self._result)

    # cmd Method
    def cmd(self, cmd='', timeout=None, strip=True, flushBefore=True):
        """
        This method sends its arguments as the command to execute and its
        arguments, then it returns the result.
        """
        if timeout == None: timeout = self.timeout

        # Manage echo behaviour on sent string length
        if self.echo:
            sentLength = len(cmd) + len(self.irs)
        else:
            sentLength = 0
        logging.debug("Length of cmd: '{}' ...".format(sentLength))

        # Make sure to flush the input before lauching any new command
        if flushBefore:
            while self.cmd(flushBefore=False): pass

        # Send the command string to the interface
        logging.debug("SENDING: '{}' ...".format(cmd))
        self.exp.send(cmd + self.ors)

        # Get the result by waiting for the prompt
        result = self.getPrompt(timeout=timeout)
        if result != None:
            result = result[sentLength:]
            if strip: result = result.strip()
        return result

    # Close
    def close(self):
        self.exp.close()

class _BaseIO(object):
    """Base class for all communication ports"""

    def __init__(self):
        self.timeout = None
        self.eof = False
        self.timedout = False
        self.buffer = ''
        self.match = None
        self.consumed = None
        self.result = None

    def buffer(self):
        return self.buffer

    def removeFirst(self, count):
        self.buffer = self.buffer[count:]

    # expect
    def expect(self, expSpecList=[], timeout=None, onEOF=None, onTimeout=None):
        # Compile any regexp in expSpecList to speed-up searches
        for expSpec in expSpecList:
            if isinstance(expSpec[0], string_types):
                expSpec[0] = re.compile(expSpec[0])

        # Initialize object properties
        self.match = None
        self.consumed = None
        self.result = None

        # Expect loop
        while True:
            # Look for a match from the list of expectation specifications
            for expSpec in expSpecList:
                # Check expectation specifications
                l = len(expSpec)
                if l == 1:
                    regexp, callback = expSpec[0], None
                elif l == 2:
                    regexp, callback = expSpec
                else:
                    raise CLIError("expect specifications require one or two "
                            + "arguments")

                # Check for a match
                match = re.search(regexp, self.buffer)
                if match != None:
                    self.match = match
                    self.result = self.buffer[:match.start()]
                    self.consumed = self.buffer[:match.end()]
                    self.buffer = self.buffer[match.end():]
                    if callback == None:
                        return True
                    else:
                        if callback(self):
                            return True

            # Fill buffer from input
            self.buffer += self.read(timeout)

            # Check for EOF
            if self.eof:
                if onEOF != None:
                    onEOF(self)
                return False

            # Check for timeout
            if self.timedout:
                if onTimeout != None:
                    onTimeout(self)
                return False

class Telnet(_BaseIO):
    def __init__(self, host, port, timeout=None):
        """Establish a telnet session.

           The host and port argument are passed to telnetlib.Telnet.

        """
        import telnetlib

        self.telnet = telnetlib.Telnet(host, port)
        self.timeout = timeout
        _BaseIO.__init__(self)

    def read(self, timeout):
        if timeout == None: timeout = self.timeout
        self.timedout = False
        (r, w, e) = select([self.telnet.fileno()], [], [], timeout)
        if len(r) == 0:
            self.timedout = True
            return ''
        try:
            s = self.telnet.read_eager()
        except EOFError:
            self.eof = True
            return ''

        # Convert bytes into strings
        if isinstance(s, bytes):
            s = s.decode('utf-8', 'replace')
        return s

    def send(self, s):
        """Send a string to the remote telnet server."""
        self.timedout = False

        # Encode strings into bytes. Telnet requires bytes (affects python 3).
        if isinstance(s, string_types):
            s = s.encode('utf-8', 'replace')
        self.telnet.write(s)

    def close(self):
        """Close the telnet session."""
        self.telnet.close()

class Serial(_BaseIO):
    """Read input from a serial port using the pySerial module.

       You should use the close() method when you want to close the
       port.  If you just discard the Serial object a file descriptor
       leak will result.
    """

    # Class properties:
    readBlkSize = 8192
    receiveTimeout = 0.05

    def __init__(self, *args, **kwargs):
        """Open a serial port using pySerial."""
        import serial

        self.serial = serial.Serial(*args, **kwargs)
        if not self.serial.isOpen:
            raise SerialError('Closed serial objects are not supported')
        _BaseIO.__init__(self)

    def read(self, timeout):
        self.timedout = False
        oldTimeout = self.serial.timeout
        self.serial.timeout = timeout

        try:
            s = self.serial.read(1)
            if len(s) == 0:
                self.timedout = True
                return ''
            self.serial.timeout = Serial.receiveTimeout
            s += self.serial.read(Serial.readBlkSize)
            self.serial.timeout = oldTimeout
        except EOFError:
            self.eof = True
            self.serial.timeout = oldTimeout
            return ''

        # Convert bytes into strings
        if isinstance(s, bytes):
            s = s.decode('utf-8', 'replace')
        return s

    def send(self, s):
        """Send a string to the serial port."""
        self.timedout = False

        # Encode strings into bytes. Serial requires bytes (affects python 3).
        if isinstance(s, string_types):
            s = s.encode('utf-8', 'replace')
        self.serial.write(s)

    def close(self):
        """Close the port."""
        self.serial.close()

class SSH(_BaseIO):
    """
    Read input from a SSH session using the paramiko module.

    Once you have created a control_cli.SSH object, you can access the
    underlying paramiko.SSHClient object via the ssh attribute, and the invoked
    shell via the shell attribute.

    You should use the close() method when you want to close the SSH session.
    If you just discard the Serial object a file descriptor leak will result.

    """
    # Class properties:
    readBlkSize = 8192
    receiveTimeout = 0.05

    def __init__(self, host, **kwargs):
        """Constructor"""
        from paramiko import SSHClient, AutoAddPolicy

        self.ssh = SSHClient()
        self.ssh.load_system_host_keys()
        self.ssh.set_missing_host_key_policy(AutoAddPolicy())
        self.ssh.connect(host, **kwargs)
        self.shell = self.ssh.invoke_shell()
        _BaseIO.__init__(self)

    def read(self, timeout):
        """_read private method"""
        self.timedout = False
        oldTimeout = self.shell.gettimeout()
        self.shell.settimeout(timeout)

        try:
            s = self.shell.recv(SSH.readBlkSize)
            self.shell.settimeout(oldTimeout)
        except socket.timeout:
            self.timedout = True
            self.shell.settimeout(oldTimeout)
            return ''

        # Convert bytes into strings
        if isinstance(s, bytes):
            s = s.decode('utf-8', 'replace')
        return s

    def send(self, s):
        """Send string to SSH shell"""
        self.timedout = False
        try:
            self.shell.sendall(s)
        except socket.timeout:
            self.timedout = True
            return False
        return True

    def close(self):
        """Close SSH session"""
        self.shell.close()
