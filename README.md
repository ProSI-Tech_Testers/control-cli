# README #

This README would normally document whatever steps are necessary to get your
application up and running.

### What is this repository for? ###

* ***control-cli*** is a pure Python module used for controlling CLI
  applications. It natively supports Telnet. Serial session uses ***pyserial***,
  and SSH connection relies on ***paramiko*** module.

* Version: 0.2.0 (Alpha)

* [GIT repository](https://bitbucket.org/ProSI-Tech_Testers/control-cli)

### How do I get set up? ###

* **If you are behind a proxy** set the following environment variables:

  - **`http_proxy`** to **`http://<your_proxy_hostname_or_ip>:<port_number>`**

  - **`https_proxy`** to
    **`https://<your_proxy_hostname_or_ip>:<port_number>`**

* Clone the repository using:  
  **`git clone https://bitbucket.org/ProSI-Tech_Testers/control-cli.git`**

* Go to module folder using:  
  **`cd control-cli`**

* Then run:  
  **`python setup.py install`**  
  This will install modules in the default system install folders which might
  require root access. You could, instead set your environment variable
  **`PYTHONPATH`** to local paths as for example:  
  **`${HOME}/.local/lib/python2.7:${HOME}/.local/lib/python2.7/site-packages`**  
  then execute:  
  **`python setup.py install --prefix=${HOME}/.local/lib`**.  
  This should automatically install required dependencies.

* ***control-cli*** depends on PyPi modules:
  [pyserial](https://pypi.python.org/pypi/pyserial),
  and [paramiko](https://pypi.python.org/pypi/paramiko).

### Contribution guidelines ###

* Development rules:

  - Syntax **shall** support both Python 2.7.x, and Python 3.6.x

  - Syntax *should* preferably support Python 2.5+, and Python 3+

  - Code **shall** be operating system agnostic, and **shall**, at least
    support Microsoft OSs, and any Unix based OS.

* Writing tests

* Code review

### Who do I talk to? ###

* Samir Khellaf [opensource@prosi-tech.com](mailto:opensource@prosi-tech.com)