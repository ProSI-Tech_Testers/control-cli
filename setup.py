#!/usr/bin/env python

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='control-cli',
    version='0.2.0',
    description='Pure Python CLI control application',
    long_description=long_description,
    url='https://bitbucket.org/ProSI-Tech_Testers/control-cli',
    author='Samir Khellaf',
    author_email='opensource@prosi-tech.com',
    license='GNU GPLv3',
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',

        'Environment :: Console',

        'Intended Audience :: Developers',
        'Intended Audience :: Telecommunications Industry',
        'Intended Audience :: System Administrators',

        'License :: License :: OSI Approved :: '
            + 'GNU General Public License v3 (GPLv3)',

        'Natural Language :: English',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',

        'Topic :: Software Development :: Testing',
        'Topic :: Terminals'
    ],
    keywords = 'expect telnet serial tcp ssh cli control',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['pyserial', 'paramiko'],
)
